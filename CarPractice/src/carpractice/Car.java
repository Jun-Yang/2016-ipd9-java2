package carpractice;

public class Car {

    public Car(String modelName, int year, double secsTo100) {
        this.modelName = modelName;
        this.year = year;
        this.secsTo100 = secsTo100;
    }

    static Car createFromLine(String line) throws InvalidInputDataException {
        String modelName; // 2-20 characters long
        int year; // 1-120
        double secsTo100; // canadian postcode

        String data[] = line.split(";");
        if (data.length != 3) {
            throw new InvalidInputDataException("Invalid data");
        } else {
            modelName = data[0];
            year = Integer.parseInt(data[1]);
            secsTo100 = Double.parseDouble(data[2]);
            return (new Car(modelName, year, secsTo100));
        }
    }

    private String modelName;
    private int year;
    private double secsTo100 ;

    @Override
    public String toString() {
        return String.format("%s is %d has %.2f", modelName, year, secsTo100);
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getSecsTo100() {
        return secsTo100;
    }

    public void setSecsTo100(double secsTo100) {
        this.secsTo100 = secsTo100;
    }

    
}

class InvalidInputDataException extends Exception {
    InvalidInputDataException(String message) {
        super(message);
    }
}
