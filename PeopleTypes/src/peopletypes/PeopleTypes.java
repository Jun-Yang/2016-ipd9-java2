/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peopletypes;

import java.util.InputMismatchException;
import java.util.Scanner;

class Person {

    private String name;
    private int age;

    public Person(String name, int age) {
        setName(name);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.length() < 2) {
            throw new IllegalArgumentException("Name must not be NULL: " + name);
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0 || age > 150) {
            throw new IllegalArgumentException("Age must be between 0-150 " + age);
        }
        this.age = age;
    }

}

class Teacher extends Person {

    private String subject;
    private int yoe;

    public Teacher(String name, int age, String subject, int yoe) {
        super(name, age);
        setSubject(subject);
        setYoe(yoe);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        if (subject == null || subject.length() < 2) {
            throw new IllegalArgumentException("Name must not be NULL: " + subject);
        }
        this.subject = subject;
    }

    public int getYoe() {
        return yoe;
    }

    public void setYoe(int yoe) {
        if (yoe < 0 || yoe > 100) {
            throw new IllegalArgumentException("Year of experience must be between 0-150 " + yoe);
        }
        this.yoe = yoe;
    }

}

class Student extends Person {

    private String program;
    private double gpa;

    public Student(String name, int age, String program, double gpa) {
        super(name, age);
        setProgram(program);
        setGpa(gpa);
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        if (program == null || program.length() < 2) {
            throw new IllegalArgumentException("Name must not be NULL: " + program);
        }
        this.program = program;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        if (gpa < 1.0 || gpa > 4.3) {
                throw new IllegalArgumentException("GPA must be between 1.0-4.3 " + gpa);
            }
        this.gpa = gpa;
    }
}

/**
 *
 * @author ipd
 */
public class PeopleTypes {
    
    private static  Scanner input = new Scanner(System.in);

    static float inputFloat(String message) {
        for (;;) {
            try {
                System.out.print(message);
                float value = input.nextFloat();
                input.nextLine(); // consume left-over new line character
                if (value > 4.3f || value < 1f) {
                    System.out.print("GPA must be between 0 and 4.3 maximum.\n");
                    continue;
                }
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }

    static int inputInt(String message) {
        for (;;) {
            try {
                System.out.print(message);
                int value = input.nextInt();
                if (value > 150 || value < 1) {
                    System.out.print("age must be between 1 and 150. \n");
                    continue;
                }
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }

    static char inputChar(String message) {
        for (;;) {
            try {
                System.out.print(message);
                char value = input.next().charAt(0);
                input.nextLine(); // consume left-over new line character
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("Please input teacher's name: ");
        String tname = input.nextLine();
        int tage = inputInt("Please input teacher's age: ");
        System.out.println("Please input teacher's subject: ");
        String subject = input.nextLine();
        int yoe = inputInt("Please input teacher's year of experience: ");
        Teacher t1 = new Teacher(tname, tage, subject, yoe);
        System.out.println("Please input teacher's name: ");
        String sname = input.nextLine();
        int sage = inputInt("Please input teacher's age: ");
        System.out.println("Please input student's program: ");
        String program = input.nextLine();
        double gpa = (double)inputFloat("Please input student's GPA: ");
        Student s1 = new Student(sname, sage, program, gpa);

        System.out.printf("The teacher's name age subject yoe are: %s %d %s %d\n",
                t1.getName(), t1.getAge(), t1.getSubject(), t1.getYoe());
        System.out.printf("The student's name age program GPA are: %s %d %s %.1f\n",
                s1.getName(), s1.getAge(), s1.getProgram(), s1.getGpa());
    }
;
}
