/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persontested;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ipd
 */
public class PersonTest {

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    public PersonTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Person.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Person instance = null;
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setName method, of class Person.
     */
    @Test
    public void testSetNameNull() throws ParseException {
        Date birthDate = formatter.parse("1979-07-18");
        String name = null;
        System.out.println("setName value is null");
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        boolean exception = false;

        try {
            instance.setName(name);
        } catch (IllegalArgumentException e) {
            exception = true;
        } catch (NullPointerException e) {
            fail("setName(Null) muest throw IllegalArgumentException, not NullPointException");
        }
        // if exception != true then fail with the message
        assertEquals("Exception expected for age " + name, exception, true);
        System.out.println("setName value is null passsed");
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test
    public void testSetNameTooShort() throws ParseException {
        Date birthDate = formatter.parse("1979-07-18");
        String name = "";
        System.out.println("setName value is too short");
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        boolean exception = false;
        try {
            instance.setName(name);
        } catch (IllegalArgumentException e) {
            exception = true;
        }

        name = "a";
        exception = false;
        try {
            instance.setName(name);
        } catch (IllegalArgumentException e) {
            exception = true;
        }
        // if exception != true then fail with the message
        assertEquals("Exception expected for age " + name, exception, true);
        System.out.println("setName value is too short passsed");
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test
    public void testSetNameRepeatA() throws ParseException {
        Date birthDate = formatter.parse("1979-07-18");
        String name = "aa";
        System.out.println("setName value is a - aaa");
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        for (int i = 3; i < 100; i++) {
            boolean exception = false;
            try {
                name += "a";
                instance.setName(name);
            } catch (IllegalArgumentException e) {
                exception = true;
            }
            // if exception != true then fail with the message
            assertEquals("Exception expected for age " + name, exception, true);
        }
        System.out.println("setName a to aaa passsed");
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getAge method, of class Person.
     */
    @Test
    public void testGetAge() {
        System.out.println("getAge");
        Person instance = null;
        int expResult = 0;
        int result = instance.getAge();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAge method, of class Person.
     */
    @Test
    public void testSetAge1to150() throws ParseException {
        Date birthDate = formatter.parse("1979-07-18");
        System.out.println("setAge...");
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        for (int age = 0; age <= 150; age++) {
            instance.setAge(age);
        }
        System.out.println("setAge 1 to 150 passsed");
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test
    public void testSetAgeOutofRangeNegative() throws ParseException {
        Date birthDate = formatter.parse("1979-07-18");
        System.out.println("setAge out of range...");
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        for (int age = -1; age > -100; age--) {
            boolean exception = false;
            try {
                instance.setAge(age);
            } catch (IllegalArgumentException e) {
                exception = true;
            }
            // if exception != true then fail with the message
            assertEquals("Exception expected for age " + age, exception, true);
        }
        System.out.println("setAge 1 to 150 passsed");
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test
    public void testSetAgeOutofRangeAbove150() throws ParseException {
        Date birthDate = formatter.parse("1979-07-18");
        System.out.println("setAge out of range...");
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        for (int age = 151; age < 250; age++) {
            boolean exception = false;
            try {
                instance.setAge(age);
            } catch (IllegalArgumentException e) {
                exception = true;
            }
            // if exception != true then fail with the message
            assertEquals("Exception expected for age " + age, exception, true);
        }
        System.out.println("setAge 1 to 150 passsed");
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of toString method, of class Person.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Person instance = null;
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPostalCode method, of class Person.
     */
    @Test
    public void testGetPostalCode() {
        System.out.println("getPostalCode");
        Person instance = null;
        String expResult = "";
        String result = instance.getPostalCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPostalCode method, of class Person.
     */
    @Test
    public void testSetPostalCode() throws ParseException {
        Date birthDate = formatter.parse("1979-07-18");
        System.out.println("setPostalCode is valid...");
        String postalCode = "7A7 A7A";
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        boolean exception = false;
        try {
            instance.setPostalCode(postalCode);
        } catch (IllegalArgumentException e) {
            exception = true;
        }
        // if exception != true then fail with the message
        assertEquals("Exception expected for age " + postalCode, exception, true);
        System.out.println("setPostalCode passsed");
    }

    /**
     * Test of getWeightKg method, of class Person.
     */
    @Test
    public void testGetWeightKg() {
        System.out.println("getWeightKg");
        Person instance = null;
        double expResult = 0.0;
        double result = instance.getWeightKg();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setWeightKg method, of class Person.
     */
    public void testSetWeight0to300() throws ParseException {
        Date birthDate = formatter.parse("1979-07-18");
        System.out.println("setWeightKg...");
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        for (int weight = 0; weight <= 300; weight++) {
            instance.setWeightKg(weight);
        }
        System.out.println("setWeightKg 0 to 300 passsed");
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test
    public void testSetWeightOutofRangeNegative() throws ParseException {
        Date birthDate = formatter.parse("1979-07-18");
        System.out.println("setWeight out of range...");
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        for (int weight = -1; weight > -100; weight--) {
            boolean exception = false;
            try {
                instance.setWeightKg(weight);
            } catch (IllegalArgumentException e) {
                exception = true;
            }
            // if exception != true then fail with the message
            assertEquals("Exception expected for weight " + weight, exception, true);
        }
        System.out.println("setWeight -1 to -100 passsed");
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test
    public void testSetWeightOutofRangeAbove300() throws ParseException {
        Date birthDate = formatter.parse("1979-07-18");
        System.out.println("setAge out of range...");
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        for (int weight = 301; weight < 500; weight++) {
            boolean exception = false;
            try {
                instance.setWeightKg(weight);
            } catch (IllegalArgumentException e) {
                exception = true;
            }
            // if exception != true then fail with the message
            assertEquals("Exception expected for weight " + weight, exception, true);
        }
        System.out.println("setWeightKg over 300 passsed");
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getDateOfBirth method, of class Person.
     */
    @Test
    public void testGetDateOfBirth() {
        System.out.println("getDateOfBirth");
        Person instance = null;
        Date expResult = null;
        Date result = instance.getDateOfBirth();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDateOfBirth method, of class Person.
     */
    @Test
    public void testSetDateOfBirth() throws ParseException {
        Date birthDate = formatter.parse("1979-01-01");
        System.out.println("setDateOfBirth is valid...");
        Person instance = new Person("Jerry", 1, "A7A 7A7", 65.0, birthDate);
        boolean exception = false;
        for (int year = 1900; year < 2100; year++) {
            try {
                birthDate = formatter.parse(year + "-02-01");
                instance.setDateOfBirth(birthDate);
            } catch (IllegalArgumentException e) {
                exception = true;
            }
            assertEquals("Exception expected for birth date " + birthDate, exception, true);
        }

        // if exception != true then fail with the message
        System.out.println("setBirthOfDate passsed");
    }

}
