/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persontested;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ipd
 */
public class Person {

    String name;
    int age;
    String postalCode; // "7A7 A7A"
    double weightKg; // 0-300 inclusive
    Date dateOfBirth; // valid date YYYY-MM-DD FROM 1900 to 2099 year

    public String getPostalCode() {
        return postalCode;
    }

    public final void setPostalCode(String postalCode) {
        String regex = "^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(postalCode);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Postalcode is not valid");
        }
        this.postalCode = postalCode;
    }

    public double getWeightKg() {
        return weightKg;
    }

    public final void setWeightKg(double weightKg) {
        if (weightKg < 0 || weightKg > 300) {
            throw new IllegalArgumentException("Weight is out of range");
        }
        this.weightKg = weightKg;
    }

    public Person(String name, int age, String postalCode, double weightKg, Date dateOfBirth) {
        setName(name);
        setAge(age);
        setPostalCode(postalCode);
        setWeightKg(weightKg);
        setDateOfBirth(dateOfBirth);
    }

    public String getName() {
        return name;
    }

    public final void setName(String name) {
        if (name == null || name.length() < 2) {
            throw new IllegalArgumentException("Name too short");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public final void setAge(int age) {
        if (age < 0 || age > 150) {
            throw new IllegalArgumentException("Invalid age");
        }
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + ", postalCode=" + postalCode + ", weightKg=" + weightKg + ", dateOfBirth=" + dateOfBirth + '}';
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public final void setDateOfBirth(Date dateOfBirth) {
        int year = dateOfBirth.getYear() + 1900;
        if ( year < 1900 || year > 2099) {
            throw new IllegalArgumentException("The year is out of range");
        }
        this.dateOfBirth = dateOfBirth;
    }

}
