/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persontested;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author ipd
 */
public class MainClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {
        ArrayList<Person> people = new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date birthDate = formatter.parse("1979-07-18");
            people.add(new Person("Jerry", 33, "A7A 7A7", 65.0, birthDate));
            people.add(new Person("Eva", 22,"A7A 7A7", 65.0, birthDate));
            people.add(new Person("Jason", 44,"A7A 7A7", 65.0, birthDate));
            people.add(new Person("Mary", 55,"A7A 7A7", 65.0, birthDate));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        for (Person p : people) {
            System.out.println(p);
        }
    }

}
