/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marine;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class Boat implements Comparable<Boat> {

    String name;
    double grossWeight;
    double length;
    int masts;

    public Boat(String name, double grossWeight, double length, int masts) {
        this.name = name;
        this.grossWeight = grossWeight;
        this.length = length;
        this.masts = masts;
    }

    @Override
    public int compareTo(Boat o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public String toString() {
//        return String.format("Ship %20s is %.1f m long and the gross weight is %.1f KG with %d masts \n",
//                name, grossWeight, length, masts);
        return String.format("%20s %10.1f %10.1f %6d \n",
                              name, grossWeight, length, masts);
    }
}

class SortByLength implements Comparator<Boat> {

    @Override
    public int compare(Boat b1, Boat b2) {

        double result = b1.length - b2.length;
        if (result == 0) {
            return 0;
        } else if (result > 0) {
            return 1;
        } else {
            return -1;
        }
    }
}

class SortByMastsWeight implements Comparator<Boat> {

    @Override
    public int compare(Boat b1, Boat b2) {

        if ((b1.masts - b2.masts) != 0) {
            return b1.masts - b2.masts;
        } else {
            double result = b1.grossWeight - b2.grossWeight;
            if (result == 0) {
                return 0;
            } else if (result > 0) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}

class SortByMasts implements Comparator<Boat> {

    @Override
    public int compare(Boat b1, Boat b2) {
        return b1.masts - b2.masts;
    }
}

/**
 *
 * @author ipd
 */
public class Marine {

    static ArrayList<Boat> boatList = new ArrayList<>();
    private final static String FILE_NAME = "input.txt";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        try (
            Scanner fileInput = new Scanner(new File(FILE_NAME))) {
            while (fileInput.hasNextLine()) {
                String fileLine = null;
                try {
                    fileLine = fileInput.nextLine();
                    String data[] = fileLine.split(";");
                    
                    if (data.length != 4) {
                        throw new IllegalArgumentException("Invalid data" + fileLine);
                    } else {
                        String name = data[0];
                        double grossWeight = Double.parseDouble(data[1]);
                        double length = Double.parseDouble(data[2]);
                        int masts = Integer.parseInt(data[3]);

                        boatList.add(new Boat(name, grossWeight, length, masts));
                    }
                } catch (IllegalArgumentException e) {
                    System.err.println("Skipping invalid input line: " + fileLine);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Cannot find the input file.");
        }

        System.out.println("======================Sort by Name======================");
        System.out.printf("%20s %10s %10s %6s \n", "Boat Name", "Gross weight", "Length","Masts");
        Collections.sort(boatList);
        for (Boat b : boatList) {
            System.out.print(b);
        }

        System.out.println("=====================Sort by length=====================");
        System.out.printf("%20s %10s %10s %6s \n", "Boat Name", "Gross weight", "Length","Masts");
        Collections.sort(boatList, new SortByLength());
        for (Boat b : boatList) {
            System.out.print(b);
        }

        System.out.println("======================Sort by masts=====================");
        System.out.printf("%20s %10s %10s %6s \n", "Boat Name", "Gross weight", "Length","Masts");
        Collections.sort(boatList, new SortByMasts());
        for (Boat b : boatList) {
            System.out.print(b);
        }
        
        System.out.println("=============Sort by masts and gross weight=============");
        System.out.printf("%20s %10s %10s %6s \n", "Boat Name", "Gross weight", "Length","Masts");
        Collections.sort(boatList, new SortByMastsWeight());
        for (Boat b : boatList) {
            System.out.print(b);
        }
    }
}
