/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3moto;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import static quiz3moto.Quiz3Moto.transportList;

class InvalidInputDataException extends Exception {

    InvalidInputDataException(String message) {
        super(message);
    }
}

class Transport {

    static Transport createFromLine(String line) throws InvalidInputDataException {
        String description; // at least 2 characters long
        int wheelsCount; // 0-100
        int maxPassengers; // 0-1000
        double maxSpeed; // 0-500
        String plates; // 3-10 characters

        String data[] = line.split(";");
        if (data.length < 4) {
            throw new InvalidInputDataException("Invalid data");
        } else {
            switch (data[0]) {
                case "Bicycle":
                    if (data.length != 4) {
                        throw new InvalidInputDataException("Invalid data");
                    } else {
                        description = data[1];
                        wheelsCount = Integer.parseInt(data[2]);
                        maxPassengers = Integer.parseInt(data[3]);
                        return (new Bicycle(description, wheelsCount, maxPassengers));
                    }
                case "Motorbike":
                    if (data.length != 6) {
                        throw new InvalidInputDataException("Invalid data");
                    } else {
                        description = data[1];
                        wheelsCount = Integer.parseInt(data[2]);
                        maxPassengers = Integer.parseInt(data[3]);
                        maxSpeed = Double.parseDouble(data[4]);
                        plates = data[5];
                        return (new Motorbike(description, wheelsCount, maxPassengers, maxSpeed, plates));
                    }
                case "Car":
                    if (data.length != 6) {
                        throw new InvalidInputDataException("Invalid data");
                    } else {
                        description = data[1];
                        wheelsCount = Integer.parseInt(data[2]);
                        maxPassengers = Integer.parseInt(data[3]);
                        maxSpeed = Double.parseDouble(data[4]);
                        plates = data[5];
                        return (new Car(description, wheelsCount, maxPassengers, maxSpeed, plates));
                    }
                case "Bus":
                    if (data.length != 6) {
                        throw new InvalidInputDataException("Invalid data");
                    } else {
                        description = data[1];
                        wheelsCount = Integer.parseInt(data[2]);
                        maxPassengers = Integer.parseInt(data[3]);
                        maxSpeed = Double.parseDouble(data[4]);
                        plates = data[5];
                        return (new Bus(description, wheelsCount, maxPassengers, maxSpeed, plates));
                    }
                default:
                    throw new InvalidInputDataException("Invalid data");
            }
        }
    }
}

class Bicycle extends Transport {

    String description; // at least 2 characters long
    int wheelsCount; // 0-100
    int maxPassengers; // 0-1000

    public Bicycle(String description, int wheelsCount, int maxPassengers) throws InvalidInputDataException {
        setDescription(description);
        setWheelsCount(wheelsCount);
        setMaxPassengers(maxPassengers);
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) throws InvalidInputDataException {
        if (description.length() < 2) {
            throw new InvalidInputDataException("The description must have at least 2 letters" + description);
        }
        this.description = description;
    }

    public int getWheelsCount() {
        return wheelsCount;
    }

    public void setWheelsCount(int wheelsCount) throws InvalidInputDataException {
        if (wheelsCount < 0 || wheelsCount > 100) {
            throw new InvalidInputDataException("The wheelsCount must between 0-100" + wheelsCount);
        }
        this.wheelsCount = wheelsCount;
    }

    public int getMaxPassengers() {

        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) throws InvalidInputDataException {
        if (maxPassengers < 0 || maxPassengers > 1000) {
            throw new InvalidInputDataException("The max Passengers must between 0-1000" + maxPassengers);
        }
        this.maxPassengers = maxPassengers;
    }

    @Override
    public String toString() {
        return String.format("Bike is %s with %d, can carry up %d  passengers\n ",
                getDescription(), getWheelsCount(), getMaxPassengers());
    }
}

class Motorbike extends Bicycle {

    double maxSpeed; // 0-500
    String plates; // 3-10 characters

    public Motorbike(String description, int wheelsCount, int maxPassengers, double maxSpeed, String plates) throws InvalidInputDataException {
        super(description, wheelsCount, maxPassengers);
        setMaxSpeed(maxSpeed);
        setPlates(plates);
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) throws InvalidInputDataException {
        if (maxSpeed < 0 || maxSpeed > 500) {
            throw new InvalidInputDataException("The max speed must between 0-500" + maxSpeed);
        }
        this.maxSpeed = maxSpeed;
    }

    public String getPlates() {
        return plates;
    }

    public void setPlates(String plates) throws InvalidInputDataException {
        if (plates.length() < 3 || plates == null) {
            throw new InvalidInputDataException("The plates must have 0-3 letters" + plates);
        }
        this.plates = plates;
    }

    @Override
    public String toString() {
        return String.format("Motobike is %s with %d wheels, can carry up %d passengers with maximum speed of %.1f km/h, registration plates %s\n",
                              getDescription(), getWheelsCount(), getMaxPassengers(), getMaxSpeed(), getPlates());
    }
}

class Car extends Motorbike {

    public Car(String description, int wheelsCount, int maxPassengers, double maxSpeed, String plates) throws InvalidInputDataException {
        super(description, wheelsCount, maxPassengers, maxSpeed, plates);
    }

    @Override
    public String toString() {
        return String.format("Car is %s with %d wheels, can carry up %d passengers with maximum speed of %.1f km/h, registration plates %s\n",
                              getDescription(), getWheelsCount(), getMaxPassengers(), getMaxSpeed(), getPlates());
    }
}

class Bus extends Motorbike {

    public Bus(String description, int wheelsCount, int maxPassengers, double maxSpeed, String plates) throws InvalidInputDataException {
        super(description, wheelsCount, maxPassengers, maxSpeed, plates);
    }

    @Override
    public String toString() {
        return String.format("Bus is %s with %d wheels, can carry up %d passengers with maximum speed of %.1f km/h, registration plates %s\n",
                              getDescription(), getWheelsCount(), getMaxPassengers(), getMaxSpeed(), getPlates());
    }
}

/**
 *
 * @author ipd
 */
public class Quiz3Moto {

    static ArrayList<Transport> transportList = new ArrayList<>();
    private final static String FILE_NAME = "input.txt";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Transport t1, maxSpeedT, maxPassengerT;
        double maxSpeed = 0;
        int maxPassenger = 0;
        double averageSpeed = 0;
        double speed = 0;
        int passenger = 0;

        maxSpeedT = new Transport();
        maxPassengerT = new Transport();
        try (
                Scanner fileInput = new Scanner(new File(FILE_NAME))) {
            while (fileInput.hasNextLine()) {
                String fileLine = fileInput.nextLine();
                try {
                    t1 = Transport.createFromLine(fileLine);
                    transportList.add(t1);
                } catch (InvalidInputDataException e) {
                    System.err.println("Invalid data " + fileLine);
                }
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Cannot find input file");
        }

        int count = 0;
        for (Transport t : transportList) {
            if (t instanceof Motorbike) {
                Motorbike m = (Motorbike) t;
                speed = m.getMaxSpeed();
                passenger = m.getMaxPassengers();
                count++;
            } else { // default
                System.out.println("Invalid data");
            }

            if (speed > maxSpeed) {
                maxSpeed = speed;
                averageSpeed += speed;
                maxSpeedT = t;
            }
            if (passenger > maxPassenger) {
                maxPassenger = passenger;
                maxPassengerT = t;
            }
        }
        averageSpeed = averageSpeed / count;                       //transportList.size();
        System.out.println("The max speed transport is:");
        System.out.print(maxSpeedT.toString());
        System.out.println("The max passenger transport is:");
        System.out.print(maxPassengerT.toString());
        System.out.printf("The average speed is:  %.1f km/h \n", averageSpeed);
    }
}
