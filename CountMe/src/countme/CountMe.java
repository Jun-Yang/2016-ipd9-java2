/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package countme;

class Box  {
    int value;
    private static int Id;
    private static int count;
    Box(){
        count++;
        Id++;
    }
    
    public static int getCount(){
        return count;
    }
    
    public static int getUniqueId(){
        return Id;
        
    }
    
            
}
/**
 *
 * @author ipd
 */
public class CountMe {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Box b1 = new Box();
        System.out.printf("The ID is: %d\n", b1.getUniqueId());      
        Box b2 = new Box();
        System.out.printf("The ID is: %d\n", b2.getUniqueId());      
        Box b3 = new Box();
        System.out.printf("The ID is: %d\n", b3.getUniqueId()); 
        System.out.printf("Count is: %d\n", Box.getCount()); 
        
    }
    
}
