package quiz2travel;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Trip {

    private String destination;
    private String travellerName;
    private String travellerPassportNo;
    private Date departureDate ;
    private Date returnDate ;

    public Trip(String destination, String travellerName, String travellerPassportNo, Date departureDate, Date returnDate) {
        this.destination = destination;
        this.travellerName = travellerName;
        this.travellerPassportNo = travellerPassportNo;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
    }

    @Override
    public String toString() {
        String departureDateStr = new SimpleDateFormat("yyyy-MM-dd").format(departureDate);
        return String.format("%s ( %s ) to  %s  on %s", travellerName,travellerPassportNo,
                destination,departureDateStr);
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTravellerName() {
        return travellerName;
    }

    public void setTravellerName(String travellerName) {
        this.travellerName = travellerName;
    }

    public String getTravellerPassportNo() {
        return travellerPassportNo;
    }

    public void setTravellerPassportNo(String travellerPassportNo) {
        this.travellerPassportNo = travellerPassportNo;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    
    
}

class InvalidInputDataException extends Exception {
    InvalidInputDataException(String message) {
        super(message);
    }
}
