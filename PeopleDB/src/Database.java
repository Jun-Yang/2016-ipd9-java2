
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ipd
 */
class SQLExceptionResultEmpty extends SQLException {}

public class Database {

    private final Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://localhost/people", "root", "root");
    }

    public void addPerson(String name, int age) throws SQLException {
        String query = "INSERT INTO person VALUES (NULL,?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, name);
        ps.setInt(2, age);
        ps.execute();
    }

    public ArrayList<Person> getAllPerson() throws SQLException {
        final String SELECT_PERSON = "SELECT * FROM PERSON ";
        ArrayList<Person> result = new ArrayList();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_PERSON);
            while (rs.next()) {
                int id = rs.getInt("idPerson");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                Person p = new Person(id, name, age);
                result.add(p);
            }
        }
        return result;
    }

    public Person getPersonByID(int id) throws SQLException {
        final String SELECT_PERSONBYID = "SELECT * FROM PERSON WHERE id" + id;
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_PERSONBYID);
            if(!rs.next()){
                throw new SQLExceptionResultEmpty();
            } 
            else{
                id = rs.getInt("idPerson");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                Person p = new Person(id, name, age);
                return p;
            }
        } 
    }
    
    public void updatePerson(Person p) throws SQLException {
        String query = "UPDATE person SET name = ? , age = ? WHERE idperson = ?";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, p.name);
        ps.setInt(2, p.age);
        ps.setInt(3, p.id);
        ps.execute();
    }

    public void deletePerson(int ID) throws SQLException {
        String query = "DELETE FROM person WHERE idperson = ?";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setInt(1, ID);
        ps.execute();
    }
}
