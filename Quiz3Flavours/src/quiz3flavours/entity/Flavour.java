/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3flavours.entity;

/**
 *
 * @author ipd
 */
public class Flavour {
    public int id;
    public String name;

    public Flavour(int id, String name) {
        setId(id);
        setName(name);
    }

    public int getId() {
        return id;
    }

    public final void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public final void setName(String name) {
        if (name == null || name.length() < 2) {
            throw new IllegalArgumentException("name is invalide");
        }
        this.name = name;
    }
    
    @Override
    public String toString() {
        return String.format("%s", name);
    }
}
