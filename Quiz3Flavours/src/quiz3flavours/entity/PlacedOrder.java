/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3flavours.entity;

/**
 *
 * @author ipd
 */
public class PlacedOrder {
    
    public int id;
    public String custName;
    public String flavourList;

    public PlacedOrder(int id, String custName, String flavourList) {
        setId(id);
        setCustName(custName);
        setFlavourList(flavourList);
    }

    public int getId() {
        return id;
    }

    public final void setId(int id) {
        this.id = id;
    }

    public String getCustName() {
        return custName;
    }

    public final void setCustName(String custName) {
        if (custName == null || custName.length() < 2) {
            throw new IllegalArgumentException(" Customer name is invalid");
        }
        this.custName = custName;
    }

    public String getFlavourList() {
        return flavourList;
    }

    public final void setFlavourList(String flavourList) {
        
        //if (flavourList == null || 
        //    (!flavourList.toUpperCase().matches("[-\\w\\s]+(,[-\\w\\s]+)*"))) {
        //    throw new IllegalArgumentException("flavourList is invalid");
        //}

        if (flavourList == null || (!flavourList.toUpperCase().matches("^$|[0-9a-zA-Z]+(,[0-9a-zA-Z]+)*"))) {
            throw new IllegalArgumentException("flavourList is invalid");
        }
        this.flavourList = flavourList;
    }
    
    @Override
    public String toString() {
        return String.format("%s ordered: %s", custName, flavourList);
    }
}
