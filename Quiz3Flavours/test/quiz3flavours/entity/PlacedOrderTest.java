/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3flavours.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ipd
 */
public class PlacedOrderTest {
    
    public PlacedOrderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class PlacedOrder.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        PlacedOrder instance = null;
        int expResult = 0;
        int result = instance.getId();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setId method, of class PlacedOrder.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 0;
        PlacedOrder instance = null;
        instance.setId(id);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCustName method, of class PlacedOrder.
     */
    @Test
    public void testGetCustName() {
        System.out.println("getCustName");
        PlacedOrder instance = null;
        String expResult = "";
        String result = instance.getCustName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCustName method, of class PlacedOrder.
     */
    @Test
    public void testSetCustNameNull() {
        System.out.println("setCustName null...");
        PlacedOrder p = new PlacedOrder(1,"Jerry","Vanila");
        //
        boolean exception = false;
        try {
            p.setCustName(null);
        } catch (IllegalArgumentException e) {
            exception = true;
        } catch (NullPointerException e) {
            fail("setCustName(null) must throw IllegalArgumentException, not NullPointerException");
        }
        assertEquals("setCustName(null) must throw exception", exception, true);
    }

    @Test
    public void testSetCustNameTooShort() {
        System.out.println("setCustName too short...");
        PlacedOrder p = new PlacedOrder(1,"Jerry","Vanila");
        //
        boolean exception = false;
        try {
            p.setCustName("");
        } catch (IllegalArgumentException e) {
            exception = true;
        }
        
        assertEquals("setCustName must throw exception", exception, true);
        //
        exception = false;
        try {
            p.setCustName("a");
        } catch (IllegalArgumentException e) {
            exception = true;
        }
        assertEquals("setCustName(\"a\") must throw exception", exception, true);
    }


    /**
     * Test of getFlavourList method, of class PlacedOrder.
     */
    @Test
    public void testGetFlavourList() {
        System.out.println("getFlavourList");
        PlacedOrder instance = null;
        String expResult = "";
        String result = instance.getFlavourList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFlavourList method, of class PlacedOrder.
     */
    @Test
    public void testSetFlavourNull() {
        System.out.println("setFlavourList null...");
        PlacedOrder p = new PlacedOrder(1,"Jerry","Vanila");
        //
        boolean exception = false;
        try {
            p.setFlavourList(null);
        } catch (IllegalArgumentException e) {
            exception = true;
        } catch (NullPointerException e) {
            fail("setFlavourList(null) must throw IllegalArgumentException, not NullPointerException");
        }
        assertEquals("setFlavourList(null) must throw exception", exception, true);
    }

    @Test
    public void testSetFlavourListValid() {
        System.out.println("setFlavourList valid...");
        PlacedOrder p = new PlacedOrder(1,"Jerry","Vanila");
        String[] valueList = {"Vanila,Orange,Apple",""};
        for (String v : valueList) {
            try {
                p.setFlavourList(v);
            } catch (IllegalArgumentException e) {
                fail("FlavourList " + v + " is valid but throws IllegalArgumentException");
            }
        }
    }

    @Test
    public void testFlavourListInvalid() {
        System.out.println("setFlavourListInvalid...");
        PlacedOrder p = new PlacedOrder(1,"Jerry","Vanila");
        String[] valueList = {null, "Vanila;Orange", "Vanila Orange"};
        for (String v : valueList) {
            boolean exception = false;
            try {
                p.setFlavourList(v);
            } catch (IllegalArgumentException e) {
                exception = true;
            }
            assertEquals("setFlavourList(" + v
                    + ") should have thrown IllegalArgumentException", exception, true);
        }
    }
    /**
     * Test of toString method, of class PlacedOrder.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        PlacedOrder instance = null;
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
