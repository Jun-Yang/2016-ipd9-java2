/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz4items;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

abstract class Item implements Comparable<Item> {

    String name;
    double weight;

    public Item(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public int compareTo(Item i) {
        return this.name.compareTo(i.name);
    } 
}

class TV extends Item {

    double diagonal;

    public TV(String name, double weight, double diagonal) {
        super(name, weight);
        this.diagonal = diagonal;
    }

    @Override
    public String toString() {
        return String.format("%20s %10.2f %10.1f \n", name, weight, diagonal);
    }
}

class Couch extends Item {
    int seats;

    public Couch(String name, double weight, int seats) {
        super(name, weight);
        this.seats = seats;
    }

    @Override
    public String toString() {
        return String.format("%20s %10.2f %10d \n", name, weight, seats);
    }
}

class Bed extends Item {

    double width, height;

    public Bed(String name, double weight, double width, double height) {
        super(name, weight);
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return String.format("%20s %10.2f %10.1f %10.1f\n", name, weight, width, height);
    }
}

class SortByWeight implements Comparator<Item> {

    @Override
    public int compare(Item i1, Item i2) {

        double result = i1.weight - i2.weight;
        if (result == 0) {
            return 0;
        } else if (result > 0) {
            return 1;
        } else {
            return -1;
        }
    }
}

class SortByNameWeight implements Comparator<Item> {

    @Override
    public int compare(Item i1, Item i2) {

        if (i1.name.compareTo(i2.name) != 0) {
            return i1.name.compareTo(i2.name);
        } else {
            double result = i1.weight - i2.weight;
            if (result == 0) {
                return 0;
            } else if (result > 0) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}

public class Quiz4Items {

    static ArrayList<Item> itemList = new ArrayList<>();
    private final static String FILE_NAME = "input.txt";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String name; // at least 2 characters long
        double weight;
        double diagonal;
        int seats;
        double width, height;

        try (
                Scanner fileInput = new Scanner(new File(FILE_NAME))) {
            while (fileInput.hasNextLine()) {
                String fileLine = null;
                try {
                    fileLine = fileInput.nextLine();
                    String data[] = fileLine.split(";");

                    switch (data[0]) {
                        case "TV":
                            if (data.length != 4) {
                                throw new IllegalArgumentException("Invalid data");
                            } else {
                                name = data[1];
                                weight = Double.parseDouble(data[2]);
                                diagonal = Double.parseDouble(data[3]);
                                itemList.add(new TV(name, weight, diagonal));
                            }
                            break;
                        case "Couch":
                            if (data.length != 4) {
                                throw new IllegalArgumentException("Invalid data");
                            } else {
                                name = data[1];
                                weight = Double.parseDouble(data[2]);
                                seats = Integer.parseInt(data[3]);
                                itemList.add(new Couch(name, weight, seats));
                            }
                            break;
                        case "Bed":
                            if (data.length != 5) {
                                throw new IllegalArgumentException("Invalid data");
                            } else {
                                name = data[1];
                                weight = Double.parseDouble(data[2]);
                                width = Double.parseDouble(data[3]);
                                height = Double.parseDouble(data[4]);
                                itemList.add(new Bed(name, weight, width, height));
                            }
                            break;
                        default:
                            throw new IllegalArgumentException("Invalid data");
                    }
                } catch (IllegalArgumentException e) {
                    System.err.println("Skipping invalid input line: " + fileLine);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Cannot find the input file.");
        }

        System.out.println("=====================Original order=====================");
        for (Item i : itemList) {
            System.out.print(i);
        }
        
        System.out.println("======================Sort by Name======================");
        Collections.sort(itemList);
        for (Item i : itemList) {
            System.out.print(i);
        }

        System.out.println("=====================Sort by weight=====================");
        Collections.sort(itemList, new SortByWeight());
        for (Item i : itemList) {
            System.out.print(i);
        }

        System.out.println("=================Sort by name and weight================");
        Collections.sort(itemList, new SortByNameWeight());
        for (Item b : itemList) {
            System.out.print(b);
        }
    }
}
