/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test1;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author ipd
 */
public class Test1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         String text;
	 PrintWriter fileOutput = null;
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input the text: ");
        text = sc.nextLine();
        sc.close();
        
        try {
            fileOutput = new PrintWriter("data.txt");
            fileOutput.println(text);
            fileOutput.close();
            File myFile = new File("data.txt");
            Scanner fileInput = new Scanner(myFile);
            while (fileInput.hasNext()) {
            text = fileInput.nextLine();
            System.out.printf(text);
            }    
            fileInput.close();
        } catch (Exception e) {
           System.out.println();
        }
    }
    
}
