/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package randarray;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author ipd
 */
public class RandArray {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("Please input the row:");
        Scanner input = new Scanner(System.in);
        int row = input.nextInt();
        System.out.println("Please input the column:");
        int column = input.nextInt();
        int[][] randArray = new int[row][column];
        System.out.println("Please input the Maximum number:");
        int maxNum = input.nextInt();
        System.out.println("Please input the Minimum number:");
        int minNum = input.nextInt();

        System.out.println("The Random array is:");
        Random r = new Random();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                randArray[i][j] = r.nextInt((maxNum - minNum) + 1) + minNum;
                System.out.printf("%3d", randArray[i][j]);
                if (j < column - 1)
                    System.out.print(',');
            }
            System.out.print('\n');
        }// TODO code application logic here
    }
}
