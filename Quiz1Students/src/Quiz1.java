/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipd
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jun
 */
public class Quiz1 {
    
     static Scanner input = new Scanner(System.in);
     private static final String FILE_NAME = "students.txt";
     
    static float inputFloat(String message) {
        for (;;) {
            try {
                System.out.print(message);
                float value = input.nextFloat();
                input.nextLine(); // consume left-over new line character
                if( value > 4.3f || value < 0f){
                    System.out.print("GPA must be between 0 and 4.3 maximum.\n");
                    continue;
                }
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }
    
    static int inputInt(String message) {
        for (;;) {
            try {
                System.out.print(message);
                int value = input.nextInt();
                input.nextLine(); // consume left-over new line character
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }
    
    static char inputChar(String message) {
        for (;;) {
            try {
                System.out.print(message);
                char value = input.next().charAt(0);
                input.nextLine(); // consume left-over new line character
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }

    public static void listStudent(){
        String name;
        float gpa;
        System.out.println("Listing all students");
        try {
            Scanner fileInput = new Scanner(new File(FILE_NAME));
            while (fileInput.hasNext()){
               name = fileInput.nextLine();
               gpa = Float.parseFloat(fileInput.nextLine());
               System.out.printf("%s has GPA %.1f\n", name, gpa);
            }
            fileInput.close();
        } catch (FileNotFoundException ex) {
            System.out.printf("System IO exception");
        }
    }
    
    public static void addStudent(){
        FileWriter filePath = null;
        String name;
        float gpa;
        System.out.println("Adding a student.");
        try {
            filePath = new FileWriter(FILE_NAME,true);
            BufferedWriter bw = new BufferedWriter(filePath);
            PrintWriter pw = new PrintWriter(bw);
            System.out.print("Enter student's name: ");
            name = input.nextLine();
            pw.println(name);
            gpa = inputFloat("Enter student's GPA: ");
            pw.println(gpa);
            pw.close();
            System.out.println("Student added.");
         } catch (IOException ex) {
            System.out.printf("System IO exception");
         } 
    }
    
    public static void findByName(){
        String name;
        float gpa;
        char firstLetter;
        firstLetter = inputChar("Enter first letter of student name (only one letter):");
        try {
            Scanner fileInput = new Scanner(new File(FILE_NAME));
            while (fileInput.hasNext()){
                name = fileInput.nextLine();
                gpa = Float.parseFloat(fileInput.nextLine());
                if(name.indexOf(firstLetter)==0){
                    System.out.println("Listing students whose names begin with letter m (case insensitive):");
                    System.out.printf("%s has GPA %.1f\n", name, gpa);
                }
            }
            fileInput.close();
         } catch (FileNotFoundException ex) {
            System.out.printf("System IO exception");
         }
    }
    
    public static void calAvgGPA(){
        String name;
        float gpa;
        float sum = 0f;
        int num = 0;
        
        try {
            Scanner fileInput = new Scanner(new File(FILE_NAME));
            while (fileInput.hasNext()){
                name = fileInput.nextLine();
                gpa = Float.parseFloat(fileInput.nextLine());
                sum = sum + gpa;
                num++;
            }
            System.out.printf("The average GPA of all students is .1%f", (sum/num));
            System.out.println();
            fileInput.close();
         } catch (FileNotFoundException ex) {
            System.out.printf("System IO exception");
         }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException  {
        int option;
        String name;
        String city;
        int age;
        
        do{
            System.out.println("1. Add student and their GPA");
            System.out.println("2. List all students and their GPAs");
            System.out.println("3. Find all students whose name begins with a letter");
            System.out.println("4. Find the average GPA of all students and display it.");
            System.out.println("0. Exit");
            
            do {
            	option = inputInt("Enter your choice:");
            } while (option < 0 && option > 4);
                
            switch(option) {
                case 1:
                    addStudent();    
                    break;
                case 2:
                    listStudent();
                    break;
                case 3:
                    findByName();
                    break;
                case 4:
                    calAvgGPA();
                    break;
                case 0:
                    System.out.println("Good bye!");
                    return;    
                default:
                    System.out.println("Invalid option. Please input a correct option (1,2,3) ");
            }
        } while(option != 0);
        input.close();
    }
}

