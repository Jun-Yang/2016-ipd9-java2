/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz2passports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.InputMismatchException;
import java.util.Scanner;

class Passport {

    private String number; // passport number AB123456 format
    private String firstName; // year of birth - between 1900-2050
    private String lastName;
    private String address;
    private String city;
    private double heightCm;
    private double weightKg;
    private int yob;

    public Passport(String number, String firstName, String lastName, String address, String city, double heightCm, double weightKg, int yob) {
        setNumber(number);
        setFirstName(firstName);
        setLastName(lastName);
        setAddress(address);
        setCity(city);
        setHeightCm(heightCm);
        setWeightKg(weightKg);
        setYob(yob);
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {

        if (!number.matches("[A-Z]{2}[0-9]{6}")) {
            throw new IllegalArgumentException("Passport code must be 2 uppercase letters with 6 numbers");
        }
        this.number = number;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        if (firstName == null || firstName.length() < 2) {
            throw new IllegalArgumentException("FirstName must be at least 2 letters: " + firstName);
        }
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        if (lastName == null || lastName.length() < 2) {
            throw new IllegalArgumentException("LastName must be at least 2 letters: " + lastName);
        }
        this.lastName = lastName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        if (address == null || address.length() < 2) {
            throw new IllegalArgumentException("Address must be at least 2 letters: " + address);
        }
        this.address = address;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        if (city == null || city.length() < 2) {
            throw new IllegalArgumentException("city must be at least 2 letters: " + city);
        }
        this.city = city;
    }

    /**
     * @return the heightCm
     */
    public double getHeightCm() {
        return heightCm;
    }

    /**
     * @param heightCm the heightCm to set
     */
    public void setHeightCm(double heightCm) {
        if (heightCm < 0 || heightCm > 300) {
            throw new IllegalArgumentException("heightCm must be between 0-300 " + heightCm);
        }
        this.heightCm = heightCm;
    }

    /**
     * @return the weightKg
     */
    public double getWeightKg() {
        return weightKg;
    }

    /**
     * @param weightKg the weightKg to set
     */
    public void setWeightKg(double weightKg) {
        if (weightKg < 0 || weightKg > 300) {
            throw new IllegalArgumentException("weightKg must be between 0-300 " + weightKg);
        }
        this.weightKg = weightKg;
    }

    /**
     * @return the yob
     */
    public int getYob() {
        return yob;
    }

    /**
     * @param yob the yob to set
     */
    public void setYob(int yob) {
        if (yob < 1900 || yob > 2050) {
            throw new IllegalArgumentException("Year of birth must be between 1900-2050 " + yob);
        }
        this.yob = yob;
    }

}

public class Quiz2Passports {

    static ArrayList<Passport> passList = new ArrayList<>();

    static final String FILE_NAME = "passports.txt";

    static void readPassports() throws FileNotFoundException {
        Scanner fileInput = new Scanner(new File(FILE_NAME));
        while (fileInput.hasNextLine()) {
            String fileLine = null;
            try {
                fileLine = fileInput.nextLine();
                /* VERSION 1
            Scanner lineInput = new Scanner(fileLine).useDelimiter(";");
            String code = lineInput.next();
            String city = lineInput.next();
            double lat = lineInput.nextDouble();
            double lgt = lineInput.nextDouble();
                 */
                String data[] = fileLine.split(";");
                if (data.length != 8) {
                    throw new IllegalArgumentException("Line malformed: " + fileLine);
                }
                String number = data[0];
                String firstName = data[1];
                String lastName = data[2];
                String address = data[3];
                String city = data[4];
                double heightCm = Double.parseDouble(data[5]);
                double weightKg = Double.parseDouble(data[6]);
                int yob = Integer.parseInt(data[7]);
                passList.add(new Passport(number, firstName, lastName, address,
                        city, heightCm, weightKg, yob));
            } catch (IllegalArgumentException e) {
                System.err.println("Skipping invalid input line: " + fileLine);
            }
        }
        fileInput.close();
    }

    private static int getMenuChoice() {
        while (true) {
            System.out.println("1. Display all passports data (one per line)\n"
                    + "2. Display all passports for people in the same city (ask for city name)\n"
                    + "3. Find the tallest person and display their info\n"
                    + "4. Find the lightest person and display their info\n"
                    + "5. Display all people younger than certain age (ask for max age, not year)\n"
                    + "6. Add person to list (in memory only)\n"
                    + "0. Save data back to file and exit.");
            System.out.print("Enter choice [0-6]: ");
            try {
                int choice = input.nextInt();
                input.nextLine();
                if (choice < 0 || choice > 6) {
                    System.out.println("Invalid choice, try again");
                } else {
                    return choice;
                }
            } catch (InputMismatchException e) {
                input.nextLine();
                System.out.println("Invalid choice, try again");
            }
        }
    }

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            readPassports();
            while (true) {
                int choice = getMenuChoice();
                switch (choice) {
                    case 0:
                        // save all airports back to file
                        saveAllPassportsToFile();
                        System.out.println("Bye bye");
                        return;
                    case 1:
                        showAllPassports();
                        break;
                    case 2:
                        findPersonsInSameCity();
                        break;
                    case 3:
                        findTallestPerson();
                        break;
                    case 4:
                        findLightesPerson();
                        break;
                    case 5:
                        findYoungerPersons();
                        break;
                    case 6:
                        addPassport();
                        break;
                    default:
                        System.out.println("Internal fatal error. Terminating.");
                        System.exit(1);
                }
            }
        } catch (IOException e) {
            System.err.println("Error reading file: " + e.getMessage());
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            System.err.println("Error reading file: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static void showAllPassports() {
        // VERSION 1
        for (int i = 0; i < passList.size(); i++) {
            Passport p = passList.get(i);
            System.out.printf("%s %s %s %s %s %.1f %.1f %d\n", p.getNumber(), p.getFirstName(), p.getLastName(),
                    p.getAddress(), p.getCity(), p.getHeightCm(), p.getWeightKg(), p.getYob());
        }
        /* // VERSION 2
        for (Passport a : passList) {
            System.out.println(a);
        }
         */
    }

    private static Passport findPassportByCity(String city) {
        for (Passport p : passList) {
            if (p.getCity().equals(city)) {
                return p;
            }
        }
        return null;
    }

    private static void findPersonsInSameCity() {
        System.out.print("Enter the city: ");
        //
        String city = input.nextLine();
        if (city.length() < 2 || city == null) {
            throw new IllegalArgumentException(" City must be at least two letter " + city);
        }
        Passport p1 = findPassportByCity(city);
        if (p1 == null) {
            System.out.println("city not found, try again.");
            return;
        }
        //
        for (Passport p : passList) {
            // skip the home airport since distance to itself is always 0
            if (p.getCity().equals(city)) {
                System.out.printf("%s %s %s %s %s %.1f %.1f %d\n", p.getNumber(), p.getFirstName(),
                        p.getLastName(), p.getAddress(), p.getCity(), p.getHeightCm(),
                        p.getWeightKg(), p.getYob());
            }
        }
    }

    private static void findYoungerPersons() {
        System.out.print("Enter the age: ");
        //
        int maxAge = input.nextInt();
        if (maxAge < 1 || maxAge > 150) {
            throw new IllegalArgumentException("Age must be between 1-150 " + maxAge);
        }
        //
        Calendar now = Calendar.getInstance();   // Gets the current date and time
        int year = now.get(Calendar.YEAR);      // The current year as an int
        System.out.printf("The persons younger than age %d are: \n", maxAge);
        for (Passport p : passList) {
            int age = year - p.getYob();
            if (age < maxAge) {
                System.out.printf("%s %s %s %s %s %.1f %.1f %d\n", p.getNumber(), p.getFirstName(),
                        p.getLastName(), p.getAddress(), p.getCity(), p.getHeightCm(),
                        p.getWeightKg(), p.getYob());
            }
        }
    }

    private static void findTallestPerson() {

        // what if first airport is home airport?
        Passport personTallestPassport = passList.get(0);
        double HeightTallest = personTallestPassport.getHeightCm();
        for (Passport p : passList) {
            // skip the home airport since distance to itself is always 0
            if (p == personTallestPassport) {
                continue;
            }
            if (HeightTallest < p.getHeightCm()) {
                HeightTallest = p.getHeightCm();
                personTallestPassport = p;
            }
        }
        System.out.printf("The tallest person is: %s %s \n", personTallestPassport.getFirstName(),personTallestPassport.getLastName());
        System.out.printf("The passpor is %s %s %s %s %s %.1f %.1f %d\n",
                personTallestPassport.getNumber(), personTallestPassport.getFirstName(),
                personTallestPassport.getLastName(), personTallestPassport.getAddress(),
                personTallestPassport.getCity(), personTallestPassport.getHeightCm(),
                personTallestPassport.getWeightKg(), personTallestPassport.getYob());

    }

    private static void findLightesPerson() {

        // what if first airport is home airport?
        Passport personLightestPassport = passList.get(0);
        double Lightest = personLightestPassport.getWeightKg();
        for (Passport p : passList) {
            // skip the home airport since distance to itself is always 0
            if (p == personLightestPassport) {
                continue;
            }
            if (Lightest > p.getWeightKg()) {
                Lightest = p.getWeightKg();
                personLightestPassport = p;
            }
        }
        System.out.printf("The lighttest person is: %s %s \n", personLightestPassport.getFirstName(),personLightestPassport.getLastName());
        System.out.printf("The passpor is %s %s %s %s %s %.1f %.1f %d\n",
                personLightestPassport.getNumber(), personLightestPassport.getFirstName(),
                personLightestPassport.getLastName(), personLightestPassport.getAddress(),
                personLightestPassport.getCity(), personLightestPassport.getHeightCm(),
                personLightestPassport.getWeightKg(), personLightestPassport.getYob());

    }

    private static void addPassport() {
        try {
            System.out.print("Enter passport number: ");
            String number = input.nextLine();
            System.out.print("Enter first name: ");
            String firstName = input.nextLine();
            System.out.print("Enter last name: ");
            String lastName = input.nextLine();
            System.out.print("Enter address: ");
            String address = input.nextLine();
            System.out.print("Enter city: ");
            String city = input.nextLine();
            System.out.print("Enter height(cm): ");
            double heightCm = input.nextDouble();
            input.nextLine();
            System.out.print("Enter weight(KG): ");
            double weightKg = input.nextDouble();
            input.nextLine();
            System.out.print("Enter year of birth: ");
            int yob = input.nextInt();
            input.nextLine();
            //
            Passport a = new Passport(number, firstName, lastName, address, city,
                    heightCm, weightKg, yob);
            passList.add(a);
        } catch (InputMismatchException | IllegalArgumentException e) {
            System.err.println("Error adding person: " + e.getMessage());
        }

    }

    private static void saveAllPassportsToFile() {
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(FILE_NAME));
            for (Passport p : passList) {
                pw.printf("%s;%s;%s;%s;%s;%.1f;%.1f;%d\n", p.getNumber(), p.getFirstName(), p.getLastName(),
                        p.getAddress(), p.getCity(), p.getHeightCm(), p.getWeightKg(),
                        p.getYob());
            }
            pw.close();
        } catch (IOException e) {
            System.err.println("Error saving to file");
            // FIXME close the file anyway
        }
    }

}
