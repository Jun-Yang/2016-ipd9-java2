/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package person1;

import java.util.ArrayList;

class Person {
    Person(String name, int age){
        this.name = name;
        this.age = age;
    }
    String name;
    int age;
}
/**
 *
 * @author ipd
 */
public class Person1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ArrayList<Person> people = new ArrayList<>();
        
        people.add(new Person("Jerry", 33));
        people.add(new Person("Maria", 23));
        people.add(new Person("Tim", 53));
        
        for(int i=0; i < people.size(); i++) {
            Person p = people.get(i);
            System.out.printf("p[%d]: name=%s, age=%d\n", i, p.name, p.age);
        }
        
        double i= 0;
        for(Person p: people){
            System.out.printf("p[%.0f]: name=%s, age=%d\n", i, p.name, p.age);
            i++; 
        }
        
        
    }
    
}
