package newpeoplelist;

public class Person {

    public Person(String name, int age, String postcode) {
        this.name = name;
        this.age = age;
        this.postcode = postcode;
    }

    static Person createFromLine(String line) throws InvalidInputDataException {
        String name; // 2-20 characters long
        int age; // 1-120
        String postcode; // canadian postcode

        String data[] = line.split(";");
        if (data.length != 3) {
            throw new InvalidInputDataException("Invalid data");
        } else {
            name = data[0];
            age = Integer.parseInt(data[1]);
            postcode = data[2];
            return (new Person(name, age, postcode));
        }
    }

    private String name;
    private int age;
    private String postcode;

    @Override
    public String toString() {
        return String.format("%s is %d y/o at %s", name, age, postcode);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

}

class InvalidInputDataException extends Exception {
    InvalidInputDataException(String message) {
        super(message);
    }
}
