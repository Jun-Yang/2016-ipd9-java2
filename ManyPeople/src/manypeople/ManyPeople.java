/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manypeople;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

class Person {

    private String name;
    private int age;

    public Person(String name, int age) {
        setName(name);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.length() < 2) {
            throw new IllegalArgumentException("Name must not be NULL: " + name);
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0 || age > 150) {
            throw new IllegalArgumentException("Age must be between 0-150 " + age);
        }
        this.age = age;
    }
    
    @Override
    public String toString() {
        return String.format("Person %s is %d years old.", getName(), getAge());
    }

}

class Teacher extends Person {

    private String subject;
    private int yoe;

    public Teacher(String name, int age, String subject, int yoe) {
        super(name, age);
        setSubject(subject);
        setYoe(yoe);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        if (subject == null || subject.length() < 2) {
            throw new IllegalArgumentException("Name must not be NULL: " + subject);
        }
        this.subject = subject;
    }

    public int getYoe() {
        return yoe;
    }

    public void setYoe(int yoe) {
        if (yoe < 0 || yoe > 100) {
            throw new IllegalArgumentException("Year of experience must be between 0-150 " + yoe);
        }
        this.yoe = yoe;
    }
    
    @Override
    public String toString() {
        return String.format("Teacher %s is %d years old and teaching %s with %d years of experience.",
                             getName(), getAge(), getSubject(), getYoe());
    }

}

class Student extends Person {

    private String program;
    private double gpa;

    public Student(String name, int age, String program, double gpa) {
        super(name, age);
        setProgram(program);
        setGpa(gpa);
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        if (program == null || program.length() < 2) {
            throw new IllegalArgumentException("Name must not be NULL: " + program);
        }
        this.program = program;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        if (gpa < 1.0 || gpa > 4.3) {
            throw new IllegalArgumentException("GPA must be between 1.0-4.3 " + gpa);
        }
        this.gpa = gpa;
    }

    @Override
    public String toString() {
        return String.format("Student %s is %d years old and studying %s with GPA %.1f",
                getName(), getAge(), getProgram(), getGpa());
    }
}

public class ManyPeople {

    static ArrayList<Person> persons = new ArrayList<>();
    static final String FILE_NAME = "people.txt";

    static void readPersons() throws FileNotFoundException {
        try (
            Scanner fileInput = new Scanner(new File(FILE_NAME))) {
            String name;
            int age;
            String subject;
            String program;
            int yoe;
            double gpa;
            
            while (fileInput.hasNextLine()) {
                String fileLine = null;
                try {
                    fileLine = fileInput.nextLine();
                    String data[] = fileLine.split(";");
                    
                    switch (data[0]) {
                        case "Person":
                            name = data[1];
                            age = Integer.parseInt(data[2]);
                            persons.add(new Person(name, age));
                            break;
                        case "Teacher":
                            name = data[1];
                            age = Integer.parseInt(data[2]);
                            subject = data[3];
                            yoe = Integer.parseInt(data[4]);
                            ;
                            persons.add(new Teacher(name, age, subject, yoe));
                            break;
                        case "Student":
                            name = data[1];
                            age = Integer.parseInt(data[2]);
                            program = data[3];
                            gpa = Double.parseDouble(data[4]);
                            ;
                            persons.add(new Student(name, age, program, gpa));
                            break;
                        default:
                            System.err.println("Skipping invalid input line: " + fileLine);
                    }
                } catch (IllegalArgumentException e) {
                    System.err.println("Skipping invalid input line: " + fileLine);
                }
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        readPersons();
        
        persons.forEach((p) -> {
            if (p instanceof Student) {
                Student s = (Student) p;
                System.out.printf("Student %s is %d years old and studying %s with GPA %.1f\n",
                        s.getName(), s.getAge(), s.getProgram(), s.getGpa());
            } else if (p instanceof Teacher) {
                Teacher t = (Teacher) p;
                System.out.printf("Teacher %s is %d years old and teaching %s with %d years of experience.\n",
                        t.getName(), t.getAge(), t.getSubject(), t.getYoe());
            } else if (p instanceof Person) {
                System.out.printf("Person %s is %d years old.\n", p.getName(), p.getAge());
            } else System.out.println("Invalid type");
        });
        
        System.out.println("The second methode:");
        persons.forEach((p) -> {
            System.out.println(p);
        });
    }
}
