/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jun
 */
public class Quiz1 {
    
     static Scanner input = new Scanner(System.in);
     private static final String FILE_NAME = "people.txt";
     
     static int inputInt(String message) {
        for (;;) {
            try {
                System.out.print(message);
                int value = input.nextInt();
                input.nextLine(); // consume left-over new line character
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input, try again");
            }
        }
    }

    public static void listPerson(){
        String name;
        String city;
        int age;
        System.out.println("Listing all persons");
        try {
            Scanner fileInput = new Scanner(new File(FILE_NAME));
            while (fileInput.hasNext()){
               name = fileInput.nextLine();
               age = Integer.parseInt(fileInput.nextLine());
               city = fileInput.nextLine();
               System.out.printf("%s is %d from %s \n", name, age, city);
            }
            fileInput.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Homework1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void addPerson(){
        FileWriter filePath = null;
        String name;
        String city;
        int age;
        System.out.println("Adding a person.");
        try {
            filePath = new FileWriter(FILE_NAME,true);
            BufferedWriter bw = new BufferedWriter(filePath);
            PrintWriter pw = new PrintWriter(bw);
            System.out.print("Enter your name:");
            name = input.nextLine();
            pw.println(name);
            age = inputInt("Enter your age:");
            pw.println(age);
            System.out.print("Enter your city:");
            city = input.nextLine();
            pw.println(city);
            pw.close();
            System.out.println("Person added.");
         } catch (IOException ex) {
            Logger.getLogger(Homework1.class.getName()).log(Level.SEVERE, null, ex);
         } 
    }
    
    public static void findByName(){
        String name;
        String city;
        int age;
        System.out.println("Enter partial person name:");
        try {
            Scanner fileInput = new Scanner(new File(FILE_NAME));
            String partialName = input.nextLine();
            while (fileInput.hasNext()){
                name = fileInput.nextLine();
                age = Integer.parseInt(fileInput.nextLine());
                city = fileInput.nextLine();
                if(name.toLowerCase().contains(partialName.toLowerCase())){
                    System.out.println("Matches found:");
                    System.out.printf("%s is %d from %s \n", name, age, city);
                }
            }
            fileInput.close();
         } catch (FileNotFoundException ex) {
            Logger.getLogger(Homework1.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    public static void findByAge(){
        String name;
        String city;
        int age;
        System.out.println("Enter maximum age:");
        try {
            Scanner fileInput = new Scanner(new File(FILE_NAME));
            int age1 = input.nextInt();
            while (fileInput.hasNext()){
                name = fileInput.nextLine();
                age = Integer.parseInt(fileInput.nextLine());
                city = fileInput.nextLine();
                if(age<age1){
                    System.out.println("Matches found:");
                    System.out.printf("%s is %d from %s \n", name, age, city);
                }
            }
            fileInput.close();
         } catch (FileNotFoundException ex) {
            Logger.getLogger(Homework1.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException  {
        int option;
        String name;
        String city;
        int age;
        
        do{
            System.out.println("1. Add person info");
            System.out.println("2. List persons info");
            System.out.println("3. Find a person by name");
            System.out.println("4. Find all persons younger than age");
            System.out.println("0. Exit");

            do {
            	option = inputInt("Enter your choice:");
            } while (option < 0 && option > 4);
                
            switch(option) {
                case 1:
                    addPerson();    
                    break;
                case 2:
                    listPerson();
                    break;
                case 3:
                    findByName();
                    break;
                case 4:
                    findByAge();
                    break;
                case 0:
                    System.out.println("Good bye!");
                    return;    
                default:
                    System.out.println("Invalid option. Please input a correct option (1,2,3) ");
            }
        } while(option != 0);
        input.close();
    }
}
