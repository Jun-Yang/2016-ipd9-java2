/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;

final class Transaction {

    private Date date;
    private String type;    // type of  transaction, such as 'W' for withdrawal, 'D' for deposit.
    private double amount;  // amount of the transaction.
    private double balance; // after this transaction.
    private String description;

    public Transaction() {
    }

    public Transaction(Date date, String type, double amount, double balance, String description) {
        setDate();
        setType(type);
        setAmount(amount);
        setBalance(balance);
        setDescription(description);
    }

    public Date getDate() {
        return date;
    }

    public final void setDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

/**
 *
 * @author ipd
 */
public class Account {

    private String customerName;
    private static int ID;
    private double balance;
    private double annualInterestRate;
    private double interestRate; // default 0;
    private static ArrayList<Transaction> transactions = new ArrayList<>();
    Date dateCreated;

    public Account() {
    }

    public Account(int id, double balance) {
        ID++;
        setBalance(balance);
    }

    public Account(String name, int id, double balance, double interest) {
        setCustomerName(name);
        ID++;
        setBalance(balance);
        setInterestRate(interest);
    }

    public Account(String customerName, int ID, double balance, ArrayList<Transaction> transactions) {
        setCustomerName(customerName);
        ID++;
        setBalance(balance);
        setTransactions(transactions);
    }

    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public void setAnnualInterestRate(double annualInterestRate) {
        if (annualInterestRate < 0f) {
            throw new IllegalArgumentException("The annual Interest Rate must be > 0");
        }
        this.annualInterestRate = annualInterestRate;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        if (interestRate < 0f) {
            throw new IllegalArgumentException("The Interest Rate must be > 0");
        }
        this.interestRate = interestRate;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public final void setDateCreated() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
        this.dateCreated = date;
    }

    public String getCustomerName() {
        return customerName;
    }

    public final void setCustomerName(String customerName) {
        if (customerName.length() < 2 || customerName == null) {
            throw new IllegalArgumentException("The holder name must not be empty");
        }
        this.customerName = customerName;
    }

    public int getID() {
        return ID;
    }

//    public final void setID(int ID) {
//        this.ID = ID;
//    }
    public double getBalance() {
        return balance;
    }

    public final void setBalance(double balance) {
        if (balance < 0f) {
            throw new IllegalArgumentException("The balance must be > 0");
        }
        this.balance = balance;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public final void setTransactions(ArrayList<Transaction> transactions) {
        this.transactions = transactions;
    }

    // returns the monthly interest rate.
    public double getMonthlyInterestRate() {
        return getAnnualInterestRate() / 12;
    }

    // returns the monthly interest. 
    public double getMonthlyInterest() {
        return getAnnualInterestRate() / 12;
    }

    public int withdraw(int amount) {
        int result = 0;
        Transaction t = new Transaction();

        t.setDate();
        t.setType("W");
        t.setAmount(amount);
        t.setDescription("Withdraw");

        if (balance > amount) {
            balance -= amount;
            t.setBalance(balance);
            transactions.add(t);
            result = 1;
        } else {
        }
        return result;
    }

    public int deposit(int amount) {
        Transaction t = new Transaction();

        balance += amount;
        t.setDate();
        t.setType("D");
        t.setAmount(amount);
        t.setDescription("Deposit");
        t.setBalance(balance);
        transactions.add(t);
        return 1;
    }

    /**
     * @param args the command line arguments Create a account with annual
     * interest rate 1.5%, balance 1000, id 1122, and name George. Deposit $30,
     * $40, and $50 to the account and withdraw $5, $4, and $2 from the account.
     * Print an account summary that shows account holder name, interest rate,
     * balance, and all transactions
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Account a = new Account("George", 1122, 1000, 1.5);
        a.deposit(30);
        a.deposit(40);
        a.deposit(50);
        a.withdraw(5);
        a.withdraw(4);
        a.withdraw(2);
        System.out.println("The information of account is:");
        System.out.printf("The holder name of account is %s\n ", a.getCustomerName());
        System.out.printf("The interest of account is %.2f\n", a.getInterestRate());
        System.out.printf("The balance of account is %.2f\n", a.getBalance());
        for (Transaction t : transactions) {
            System.out.printf("The transactin is %s %s %.2f %.2f %s\n", t.getDate(), t.getType(), t.getAmount(), t.getBalance(), t.getDescription());
        }

    }

}
