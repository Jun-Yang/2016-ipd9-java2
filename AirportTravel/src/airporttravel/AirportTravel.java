/**
 *
 * @author Jun Yang
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airporttravel;

import static airporttravel.AirportTravel.input;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author Jun
 */

class Airport {
    private String code, city;
    private double latitude, longitude;

    Airport() {
        setCode("   ");
        setCity("   ");
        setLatitude(0);
        setLongitude(0);
    }    
    
    Airport(String code, String city, double latitude, double longitude) {
        setCode(code);
        setCity(city);
        setLatitude(latitude);
        setLongitude(longitude);
    }    
    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        if( code.length() != 3 ){
            throw new IllegalArgumentException("Code Length must be 3");
        } 
        this.code = code.toUpperCase();
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        if(city.isEmpty() || city == null){
            throw new IllegalArgumentException("Name cannot be empty");
        } 
        this.city = city;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        if( latitude < -90 || latitude > 90){
            throw new IllegalArgumentException("Latitude must be between -90 and 90");
        } 
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        if( longitude < -180 || longitude > 180){
            throw new IllegalArgumentException("Longitude must be between -180 and 180");
        } 
        this.longitude = longitude;
    }
}

public class AirportTravel {
    
     static Scanner input = new Scanner(System.in).useLocale(Locale.US);;
     static ArrayList<Airport> airports = new ArrayList<>();
     private static final String FILE_NAME = "airports.txt";
     private static final double MAX_LONGITUDE = 180;
     private static final double MIN_LONGITUDE = -180;
     private static final double MAX_LATITUDE = 90;
     private static final double MIN_LATITUDE = -90;
     private static final String DELIMETER = ";";
     
    static float inputFloat(String message) {
        for (;;) {
            try {
                System.out.print(message);
                float value = input.nextFloat();
                input.nextLine(); // consume left-over new line character
                if( value > 4.3f || value < 0f){
                    System.out.print("GPA must be between 0 and 4.3 maximum.\n");
                    continue;
                }
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }
    static double inputLogitude(String message) {
        double value;
        for (;;) {
            try {
                System.out.print(message);
                value = input.nextDouble();
                input.nextLine(); // consume left-over new line character
                if( value > MAX_LONGITUDE || value < MIN_LONGITUDE){
                    System.out.printf("Longitude must be between %f and %f .\n", MAX_LONGITUDE,MIN_LONGITUDE);
                    continue;
                }
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }
    
    static double inputLatitude(String message) {
        double value;
        for (;;) {
            try {
                System.out.print(message);
                value = input.nextDouble();
                input.nextLine(); // consume left-over new line character
                if( value > MAX_LATITUDE || value < MIN_LATITUDE){
                    System.out.printf("Latitude must be between %f and %f .\n", MAX_LATITUDE,MIN_LATITUDE);
                    continue;
                }
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }
    static String inputCode(String message) {
        for (;;) {
            try {
                System.out.print(message);
                String value = input.next();
                input.nextLine(); // consume left-over new line character
                if( value.length() != 3){
                    System.out.print("The length of code must be 3.\n");
                    continue;
                }
                return value.toUpperCase();
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }
    
    static String inputCity(String message) {
        for (;;) {
            try {
                System.out.print(message);
                String value = input.nextLine();
                value = value.toLowerCase(); 
                value = value.substring(0, 1).toUpperCase() + value.substring(1).toLowerCase();
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }
    
    static int inputInt(String message) {
        for (;;) {
            try {
                System.out.print(message);
                int value = input.nextInt();
                input.nextLine(); // consume left-over new line character
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }
    
    static char inputChar(String message) {
    	String name;
        for (;;) {
            try {
                System.out.print(message);
                name = input.next();
                if(name.length()>1) {
                	input.nextLine(); // consume the invalid input
                    System.out.println("Invalid input");
                    continue;
                }
                char value = name.charAt(0);
                input.nextLine(); // consume left-over new line character
                return value;
            } catch (InputMismatchException ime) {
                input.nextLine(); // consume the invalid input
                System.out.println("Invalid input");
            }
        }
    }
    
    static double computeDistance (Airport a1, Airport a2){
        double theta = a1.getLongitude() - a2.getLongitude();
        double dist = Math.sin(deg2rad(a1.getLatitude())) * Math.sin(deg2rad(a2.getLatitude())) 
                      + Math.cos(deg2rad(a1.getLatitude())) * Math.cos(deg2rad(a2.getLatitude())) 
                      * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515 *1.6;
        return dist;
    }
    
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::	This function converts decimal degrees to radians						 :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
            return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::	This function converts radians to decimal degrees						 :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
            return (rad * 180 / Math.PI);
    }
    
     public static void readAirports(){
        String line;
        double latitude, longitude;
        String code, city;
        String[] temp;
        Airport a1 = new Airport();
        
        try {
            Scanner fileInput = new Scanner(new File(FILE_NAME));
            while (fileInput.hasNext()){
               line = fileInput.nextLine();
               temp = line.split(DELIMETER);
               code = temp[0];
               city = temp[1];
               latitude = Double.parseDouble(temp[2]);
               longitude = Double.parseDouble(temp[3]);
               airports.add(new Airport(code, city, latitude, longitude));
            }
            fileInput.close();
        } catch (FileNotFoundException ex) {
            System.out.println("System IO exception");
        }
    }
     
    public static void showAirports(){
        
        readAirports();
        System.out.println("Listing all airports: ");
        for (Airport a1 : airports){
            System.out.printf("%s %s %f %f \n", a1.getCode(), a1.getCity(), a1.getLatitude(), a1.getLongitude());
        }
    }
    
    public static void addAirports(){
        FileWriter filePath = null;
        String code, city;
        double latitude,longitude;
        String line;
        
        System.out.println("Adding a airport.");
        try {
            filePath = new FileWriter(FILE_NAME,true);
            BufferedWriter bw = new BufferedWriter(filePath);
            PrintWriter pw = new PrintWriter(bw);
            code = inputCode("Please input the code: ");
            city = inputCity("Please input the city: ");
            latitude = inputLatitude("Please input the latitude: ");
            longitude = inputLogitude("Please input the logitude: ");
            line = code + DELIMETER + city + DELIMETER + latitude + DELIMETER + longitude;
            pw.println(line);
            pw.close();
            System.out.println("Airport added.");
         } catch (IOException ex) {
            System.out.println("System IO exception");
         } 
    }
    
    public static Airport findByCode(String code1){
        String code;
        
        for (Airport a1 : airports){
            code = a1.getCode();
            if (code.equals(code1)) {
              return a1;  
            }
        }
        System.out.println("Invalid code");
        return null;
    }
    public static Airport findByCity(String city1){
        String city;
        
        for (Airport a1 : airports){
            city = a1.getCity().toUpperCase();
            if (city.equals(city1.toUpperCase())) {
              return a1;  
            }
        }
        System.out.println("Invalid code");
        return null;
    }
    
    public static void findDistance(){
        String firstCode, secondCode;
        Airport a1 = new Airport();
        Airport a2 = new Airport();
        double distance;
        
        readAirports();
        firstCode = inputCode("Enter the first code of airport (only 3 letter):");
        secondCode = inputCode("Enter the second code of airport (only 3 letter):");
        a1 = findByCode(firstCode);
        if(a1==null){
            System.out.println("Cannot find the code");
            return;
        }
        a2= findByCode(secondCode);
        if(a2==null){
            System.out.println("Cannot find the code");
            return;
        }
        distance = computeDistance(a1,a2);
        System.out.printf("The distance between %s and %s is %.0fKM\n", firstCode, secondCode, distance);
    }           
    
    public static void findAirports(){
        String name;
        String city1 = null,city2 = null;
        Airport a1 = new Airport();
        double distance = 0;
        double nearest = 10000000, secondNearest = 10000000;
        
        readAirports();
        name = inputCity("Enter the city: ");
        a1 = findByCity(name);
        if(a1==null){
            System.out.println("Cannot find the city");
            return;
        }
        for (Airport a2 : airports){
            if (a1.getCity() != a2.getCity()) {
                    distance = computeDistance(a1,a2);
                    if(distance == nearest){
                        secondNearest = nearest;
                    } else if (distance < nearest) {
                        secondNearest = nearest;
                        nearest = distance;
                        city1 = a2.getCity();
                    } else if (distance < secondNearest) {
                        secondNearest = distance;
                        city2 = a2.getCity();
                    }
            }
        }
        System.out.printf("The nearest airport to %s are %s and %s \n", a1.getCity(), city1, city2);
        System.out.printf("The distance are %.0f and %.0f \n", nearest, secondNearest);
    }           
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException  {
        int option;
                
        do{
            System.out.println("1. Show all airports (codes and city names)");
            System.out.println("2. Find distance between two airports by codes.");
            System.out.println("3. Find the 2 airports nearest to an airport given.");
            System.out.println("4. Add a new airport to the list.");
            System.out.println("0. Exit");
            
            do {
            	option = inputInt("Enter your choice: ");
            	if (option < 0 || option > 4) 
            		System.out.println("Invalid choice, try again.");
            } while (option < 0 && option > 4);
                
            switch(option) {
                case 1:
                    showAirports();    
                    break;
                case 2:
                    findDistance();
                    break;
                case 3:
                    findAirports();
                    break;
                case 4:
                    addAirports();
                    break;
                case 0:
                    System.out.println("Good bye!");
                    return;    
                default:
                    break;
            }
        } while(option != 0);
        input.close();
    }
}