/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demandingpeople;
/**
 *
 * @author ipd
 */

class Person {
    Person(String name, int age){
        setName(name);
        setAge(age);
    }
    private String name;
    private int age;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        if(name.isEmpty() || name == null){
            throw new IllegalArgumentException("Name cannot be empty");
        } 
        this.name = name;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        if( age < 1 || age > 150){
            throw new IllegalArgumentException("Age must be 1-150");
        } 
        this.age = age;
    }
}

public class DemandingPeople {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            Person p = new Person("Jerry",-33);
//            p.setAge(101);
            p.setName(null);
//            p.setName("");
            System.out.printf("p: name=%s, age=%d\n", p.getName(), p.getAge());
        // TODO code application logic here
        } catch (IllegalArgumentException ipe ){
            System.out.println("Invalid parameter " + ipe.getMessage());
        }
    }
}
