/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicrm.entity;

import java.sql.Date;

/**
 *
 * @author ipd
 */
public class Interaction {
    int interactionID;
    int customerID;
    int repID;
    Date date;
    String notes;

    public Interaction(int interactionID, int customerID, int repID, Date date, String notes) {
        this.interactionID = interactionID;
        this.customerID = customerID;
        this.repID = repID;
        this.date = date;
        this.notes = notes;
    }

    
    
}
