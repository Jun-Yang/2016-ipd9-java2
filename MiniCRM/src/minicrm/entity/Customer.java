/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicrm.entity;

/**
 *
 * @author ipd
 */
public class Customer {
    int customerID;
    String nameFirst;
    String nameLast;
    String address;
    String postalCode;
    String phoneNumber;
    String SIN;

    public Customer(int customerID, String nameFirst, String nameLast, String address, String postalCode, String phoneNumber, String SIN) {
        this.customerID = customerID;
        this.nameFirst = nameFirst;
        this.nameLast = nameLast;
        this.address = address;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.SIN = SIN;
    }

    @Override
    public String toString() {
        return String.format("%d %s %s %s %s %s %s", customerID, nameFirst,nameLast, address, postalCode, phoneNumber, SIN);
    }
    
}
