/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicrm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import minicrm.entity.Customer;
import minicrm.entity.Representative;

/**
 *
 * @author ipd
 */
public class Database {
    private Connection conn;
    
    public Database() throws SQLException {
        conn = DriverManager.getConnection("jdbc:sqlite:minicrm.db");
    }
    
    public void addCustomer(String nameFirst, String nameLast, String address, String postalCode, String phoneNumber, String SIN) throws SQLException {
        String query = "INSERT INTO customer VALUES (NULL,?,?,?,?,?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, nameFirst);
        ps.setString(2, nameLast);
        ps.setString(3, address);
        ps.setString(4, postalCode);
        ps.setString(5, phoneNumber);
        ps.setString(6, SIN);
        ps.execute();
    }

    public ArrayList<Customer> getAllCustomer() throws SQLException {
        final String SELECT_CUSTOMER = "SELECT * FROM customer ";
        ArrayList<Customer> result = new ArrayList();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_CUSTOMER);
            while (rs.next()) {
                int id = rs.getInt("CustomerID");
                String nameFirst = rs.getString("nameFirst");
                String nameLast = rs.getString("nameLast");
                String address = rs.getString("address");
                String postalCode = rs.getString("postalCode");
                String phoneNumber = rs.getString("phoneNumber");
                String SIN = rs.getString("SIN");
                Customer c = new Customer(id, nameFirst, nameLast, address, postalCode,phoneNumber,SIN);
                result.add(c);
            }
        }
        return result;
    }
    
    public void addRepresentative(String nameFirst, String nameLast) throws SQLException {
        String query = "INSERT INTO representative VALUES (NULL,?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, nameFirst);
        ps.setString(2, nameLast);        
        ps.execute();
    }

    public ArrayList<Representative> getAllRepresentative() throws SQLException {
        final String SELECT_REPRESENTATIVE = "SELECT * FROM representative ";
        ArrayList<Representative> result = new ArrayList();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_REPRESENTATIVE);
            while (rs.next()) {
                int id = rs.getInt("representiveID");
                String nameFirst = rs.getString("nameFirst");
                String nameLast = rs.getString("nameLast");
                Representative r = new Representative(id, nameFirst, nameLast);
                result.add(r);
            }
        }
        return result;
    }
}
