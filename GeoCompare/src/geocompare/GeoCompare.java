/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geocompare;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

abstract class GeoObj implements Comparable<GeoObj> {

    private String color;

    public GeoObj(String color) {
        setColor(color);
    }

    public String getColor() {
        return color;
    }

    public final void setColor(String color) {
        if (color == null || color.length() < 1) {
            throw new IllegalArgumentException("The color must be at least one letter");
        }
        this.color = color;
    }
    
    @Override
    public int compareTo(GeoObj g){
        // Comparing this to g
        System.out.printf("Comparing %s to %s\n", this, g);
        if ((this.getSurface()) == g.getSurface()){
            return 0;
        } else if ((this.getSurface()) > g.getSurface()){
            return 1;
        } else {
            return -1;
        }
    }
    
    abstract public double getSurface();

    abstract public double getCircumference();

    abstract public int getVerticesCount();

    abstract public int getEdgesCount();

    abstract public void print();
}

class Point extends GeoObj {

    public Point(String color) {
        super(color);
    }

    @Override
    public double getSurface() {
        System.out.printf("The surface of point is %.2f \n", 0f);
        return 0;
    }

    @Override
    public double getCircumference() {
        System.out.printf("The circumference of point is %.2f \n", 0f);
        return 0;
    }

    @Override
    public int getVerticesCount() {
        System.out.printf("The vertices count of point is %d \n", 1);
        return 1;
    }

    @Override
    public int getEdgesCount() {
        System.out.printf("The edges count of point is %d \n", 1);
        return 1;
    }

    @Override
    public void print() {
        System.out.printf("This is a %s point\n", getColor());
        getSurface();
        getCircumference();
        getVerticesCount();
        getEdgesCount();
    }
}

class Rectangle extends GeoObj {

    private double width;
    private double height;

    Rectangle(String color, double width, double height) {
        super(color);
        setWidth(width);
        setHeight(height);
    }

    public double getWidth() {
        return width;
    }

    public final void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public final void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getSurface() {
        System.out.printf("The surface of rectangle is %.2f \n", width * height);
        return width * height;
    }

    @Override
    public double getCircumference() {
        System.out.printf("The circumference of rectangle is %.2f \n", 2 * width + 2 * height);
        return 2 * width + 2 * height;
    }

    @Override
    public int getVerticesCount() {
        System.out.printf("The vertices count of rectangle is %d \n", 4);
        return 4;
    }

    @Override
    public int getEdgesCount() {
        System.out.printf("The edges count of rectangle is %d \n", 4);
        return 4;
    }

    @Override
    public void print() {
        System.out.printf("This is a %s rectangle\n", getColor());
        getSurface();
        getCircumference();
        getVerticesCount();
        getEdgesCount();
    }
}

class Square extends Rectangle {

    private double edgeSize;

    Square(String color, double edgeSize) {
        super(color, edgeSize, edgeSize);
    }

    public double getEdgeSize() {
        return edgeSize;
    }

    public final void setEdgeSize(double edgeSize) {
        this.edgeSize = edgeSize;
    }

    @Override
    public void print() {
        System.out.printf("This is a %s square\n", getColor());
        getSurface();
        getCircumference();
        getVerticesCount();
        getEdgesCount();
    }
}

class Circle extends GeoObj {

    private double radius;

    Circle(String color, double radius) {
        super(color);
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public final void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getSurface() {
        System.out.printf("The surface of circle is %.2f \n", radius * radius * Math.PI);
        return radius * radius * Math.PI;
    }

    @Override
    public double getCircumference() {
        System.out.printf("The circumference of circle is %.2f \n", 2 * radius * Math.PI);
        return 2 * radius * Math.PI;
    }

    @Override
    public int getVerticesCount() {
        System.out.printf("The vertices count of circle is %d \n", 0);
        return 0;
    }

    @Override
    public int getEdgesCount() {
        System.out.printf("The edges count of circle is %d \n", 1);
        return 1;
    }

    @Override
    public void print() {
        System.out.printf("This is a %s circle\n", getColor());
        getSurface();
        getCircumference();
        getVerticesCount();
        getEdgesCount();
    }
}

class Sphere extends GeoObj {

    private double radius;

    Sphere(String color, double radius) {
        super(color);
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public final void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getSurface() {
        System.out.printf("The surface of sphere is %.2f \n", radius * radius * Math.PI * 4);
        return radius * radius * Math.PI * 4;
    }

    @Override
    public double getCircumference() {
        System.out.printf("The circumference of sphere is %.2f \n", 2 * radius * Math.PI);
        return 2 * radius * Math.PI;
    }

    @Override
    public int getVerticesCount() {
        System.out.printf("The vertices count of sphere is %d \n", 0);
        return 0;
    }

    @Override
    public int getEdgesCount() {
        System.out.printf("The edges count of sphere is %d \n", 1);
        return 1;
    }

    @Override
    public void print() {
        System.out.printf("This is a %s sphere\n", getColor());
        getSurface();
        getCircumference();
        getVerticesCount();
        getEdgesCount();
    }
}

/**
 *
 * @author ipd
 */
public class GeoCompare {

    static ArrayList<GeoObj> shapes = new ArrayList<>();
    private final static String FILE_NAME = "geoobj.txt";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        try (
            Scanner fileInput = new Scanner(new File(FILE_NAME))){
            String color;
            double width, height;
            double edgeSize;
            int age;
            String subject;
            String program;
            int yoe;
            double gpa;
            double radius;

            while (fileInput.hasNextLine()) {
                String fileLine = null;
                try {
                    fileLine = fileInput.nextLine();
                    String data[] = fileLine.split(";");
                    if (data.length < 2) {
                        System.err.println("Skipping invalid input line: " + fileLine);
                    } else {
                        switch (data[0]) {
                            case "Point":
                                color = data[1];
                                shapes.add(new Point(color));
                                break;
                            case "Rectangle":
                                color = data[1];
                                width = Double.parseDouble(data[2]);
                                height = Double.parseDouble(data[3]);
                                shapes.add(new Rectangle(color, width, height));
                                break;
                            case "Square":
                                color = data[1];
                                edgeSize = Double.parseDouble(data[2]);
                                shapes.add(new Square(color, edgeSize));
                                break;
                            case "Circle":
                                color = data[1];
                                radius = Double.parseDouble(data[2]);
                                shapes.add(new Circle(color, radius));
                                break;
                            case "Sphere":
                                color = data[1];
                                radius = Double.parseDouble(data[2]);
                                shapes.add(new Sphere(color, radius));
                                break;
                            default:
                                System.err.println("Skipping invalid input line: " + fileLine);
                                break;
                        }
                    }
                } catch (IllegalArgumentException e) {
                    System.err.println("Skipping invalid input line: " + fileLine);
                }
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Cannot find the input file.");
        }

        for (GeoObj obj : shapes) {
            obj.print();
        }
        
        // sorted
        Collections.sort(shapes);
        System.out.println("===========sorted=============");
        for (GeoObj obj : shapes) {
            obj.print();
        }
    }

}
