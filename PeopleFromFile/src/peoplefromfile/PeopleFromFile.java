/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peopleFromFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

class Person {
    Person(String name, int age){
        this.name = name;
        this.age = age;
    }
    String name;
    int age;
}
/**
 *
 * @author ipd
 */
public class PeopleFromFile {
    private static final String FILE_NAME = "people.txt";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ArrayList<Person> people = new ArrayList<>();
        
        try {
             Scanner fileInput = new Scanner(new File(FILE_NAME));
             while (fileInput.hasNext()){
                 String name = fileInput.nextLine();
                 int age = Integer.parseInt(fileInput.nextLine());
                 people.add(new Person(name, age));
             }
             fileInput.close();
          } catch (FileNotFoundException ex) {
             System.out.printf("System IO exception");
          }

         for(int i=0; i < people.size(); i++) {
             Person p = people.get(i);
             System.out.printf("p[%d]: name=%s, age=%d\n", i, p.name, p.age);
         }

         int i= 0;
         for(Person p: people){
             System.out.printf("p[%d]: name=%s, age=%d\n", i, p.name, p.age);
             i++; 
         }
    }
}
