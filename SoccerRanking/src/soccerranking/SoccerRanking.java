/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soccerranking;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class Team implements Comparable<Team> {

    String title;
    int matchesWon;
    int matchesLost;
    
    public double getPercentage(){
       double percentage = (double) this.matchesWon/(this.matchesLost + this.matchesWon) * 100;
       return percentage;
    }

    public Team(String title, int matchesWon, int matchesLost) {
        this.title = title;
        this.matchesWon = matchesWon;
        this.matchesLost = matchesLost;
    }

    @Override
    public int compareTo(Team t) {
        // Comparing this to g
        
//        if (this.matchesWon == t.matchesWon) {
//            return 0;
//        } else if ((this.matchesWon) > t.matchesWon) {
//            return 1;
//        } else {
//            return -1;
//        }
        int result = this.matchesWon - t.matchesWon;
        if (result != 0) return result;
        return this.matchesLost - t.matchesLost;
    }
    
    @Override 
    public String toString(){
        double percentage = (double) matchesWon/(matchesLost + matchesWon) * 100;
        return String.format("Team %s: won %d  lost %d  %.2f \n",
                              title, matchesWon,matchesLost,percentage);
    }
}

class SortByName implements Comparator <Team> {

    @Override
    public int compare(Team o1, Team o2) {
        return o1.title.compareTo(o2.title);
    }
}

class SortByPercentage implements Comparator <Team> {

    @Override
    public int compare(Team o1, Team o2) {
        
        double result = o1.getPercentage() - o2.getPercentage();
        if (result == 0)  return 0;
        else if (result > 0) return 1;
        else return -1;
    }
}

/**
 *
 * @author ipd
 */
public class SoccerRanking {

    static ArrayList<Team> teamList = new ArrayList<>();
    private final static String FILE_NAME = "input.txt";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Team t = null;

        try (
            Scanner fileInput = new Scanner(new File(FILE_NAME))) {
            while (fileInput.hasNextLine()) {
                String fileLine = null;
                try {
                    fileLine = fileInput.nextLine();
                    String data[] = fileLine.split(";");
                    if (data.length != 3) {
                        throw new IllegalArgumentException("Invalid data" + fileLine);
                    } else {
                        String title = data[0];
                        int matchesWon = Integer.parseInt(data[1]);
                        int matchesLost = Integer.parseInt(data[2]);
                        
                        teamList.add(new Team(title,matchesWon,matchesLost));
                    }
                } catch (IllegalArgumentException e) {
                    System.err.println("Skipping invalid input line: " + fileLine);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Cannot find the input file.");
        }
        
        System.out.printf("%10s %4s %4s \n", "Team", "Won", "Lost");
        Collections.sort(teamList);
        for (Team t1 : teamList) {
            System.out.printf("%10s %4d %4d \n", t1.title, t1.matchesWon, t1.matchesLost);
        }
        
        System.out.println("=================Sort by Name=================");
        Collections.sort(teamList, new SortByName());
        for (Team t1 : teamList) {
            System.out.printf("%10s %4d %4d \n", t1.title, t1.matchesWon, t1.matchesLost);
        }
        
        System.out.println("=================Sort by Percentage=================");
        Collections.sort(teamList, new SortByPercentage());
        for (Team t1 : teamList) {
            System.out.print(t1);
        }
    }
}
